namespace authService.Authentication  
{  
    public static class UserRoles  
    {  
        public const string Admin = "Admin";  
        public const string hospitalAdmin = "Hospital Administrator";  
        public const string businessOffice = "Business Office";  
        public const string inventoryManager = "Inventory Manager";  
        public const string patientAdmin = "Patient Administrator";  
        public const string doctor = "Doctor";  
        public const string nurse = "Nurse";  
        public const string nurseManager = "Nurse Manager";  
        public const string socialWorker = "Social Worker";  
        public const string imagingTech = "Imaging Technician";  
        public const string labTech = "Lab Technician";  
        public const string pharmacist = "Pharmacist";  
        
        public const string User = "User";  
        public const string patient = "Patient";  
    }  
}  